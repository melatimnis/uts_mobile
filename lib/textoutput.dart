import 'package:flutter/material.dart';
import './textcontrol.dart';

class TextOutput extends StatefulWidget{
  @override
  _TextOutputState createState() => _TextOutputState();
}

class _TextOutputState extends State<TextOutput>{
  String msg = 'Coffe blues Jogja';

  void _changeText() {
    setState(() {
      if (msg.startsWith('L')) {
        msg = 'Coffe Blues Jogja';
      }else{
        msg = 'Liburan Indie';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Lagu Favoritku ', style: new TextStyle(fontSize:30.0),),
          TextControl(msg),
          RaisedButton(child: Text("Ganti Lagu",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}